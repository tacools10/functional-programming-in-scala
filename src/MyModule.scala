// A comment!
/* Another comment */
/**
 * A documentation comment
 */

/**
 * object declares a singleton, class and instance declared simultaneously
 * often used where you would use a class with static members in Java
 */
object MyModule {

  /**
   * This is a higher order function HOF that takes a function
   * mapping Int to Int as an input
   * @param name
   * @param n
   * @param f is a function mapping Int to Int. Naming convention in Scala
   *          is f, g, h for functions
   * @return
   */
  def formatResult(name: String, n: Int, f: Int => Int): String = {
    val msg = "The %s of %d is %d"
    msg.format(name, n, f(n))
  }

  def fibonnaci(n: Int): Int = {

    @scala.annotation.tailrec
    def fib_tail(n: Int, a: Int, b: Int): Int = n match {
      case 0 => a
      case _  => fib_tail(n - 1, b, a + b)
    }

    fib_tail(n, 0 , 1)

  }


  def factorial(n: Int): Int = {
    /**
     * This is an inner function or local definition.
     * Like a local variable in a non-fp language
     * @param n
     * @param acc
     * @return
     */
    @annotation.tailrec
    def go(n: Int, acc: Int): Int =
      if (n <= 0) acc
      else go(n - 1, n * acc)

    /**
     * These are the initial arguments for state of loop
     * Scala converts the bytecode to while loop as
     * recursion is in tail position
     */
    go(n, 1)
  }
  /**
   *
   * @param n an integer
   * @return returns the absolute value
   */
  def abs(n: Int): Int =
    if (n < 0) -n
    else n

  /**
   * Private method can only be called my other
   * members of MyModule (like Java)
   * @param x
   * @return
   */
  private def formatAbs(x: Int) = {
    val msg = "The absolute value of %d is %d"
    msg.format(x, abs(x))
  }

  private def formatFactorial(n: Int) = {
    val msg = "The factorial of %d is %d."
    msg.format(n, factorial(n))
  }

  /**
   * Main method, Unit is equivalent to void in Java
   * @param args
   */
  def main(args: Array[String]): Unit =
   /* println(formatAbs(-42))

    println(formatFactorial(10))

    println(formatResult("factorial", 7, factorial))
    println(formatResult("abs", -1000, abs)) */

    println(fibonnaci(105))
}
