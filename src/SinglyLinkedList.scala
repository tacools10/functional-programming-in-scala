sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List {

  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product(xs)
  }

  def apply[A](as: A*): List[A] =
    if (as.isEmpty) Nil
    else (Cons(as.head, apply(as.tail: _*)))

  def tail[A](list : List[A]): List[A] = list match {
    case Nil => Nil
    case Cons(_, xs) => xs
  }

  def setHead[A](element: A, list: List[A]): List[A] = list match {
    case Nil => Cons(element, Nil)
    case Cons(_, xs) => Cons(element, xs)
  }

  @scala.annotation.tailrec
  def dropAnswer[A](l: List[A], n: Int): List[A] =
    if (n <= 0) l
    else l match {
      case Nil => Nil
      case Cons(_, t) => dropAnswer(t, n-1)
    }

  @scala.annotation.tailrec
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
    case Cons(h, t) if f(h) => dropWhile(t, f)
    case _ => l
  }



  def main(args: Array[String]): Unit = {

    val x = List(1, 2, 3, 4, 5) match {
      case Cons(x, Cons(2, Cons(4, _))) => x
      case Nil => 42
      case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
      case Cons(h, t) => h + sum(t)
      case _ => 101
    }

    // Case 3 is the first case that matches so x = 3 (1 + 2)
    System.out.println(x);

    val y = List(1, 3, 3, 5)

    val z = List(Nil)

    val xy = List(1, 3,3 ,3, 5 ,6, 66,6,66,6)

    System.out.println(tail(y).toString)

    System.out.println(setHead(15, y))

    System.out.println(setHead(30, z).toString)

<<<<<<< HEAD
    // System.out.println(drop(5, xy));

    System.out.println("Doing dropWhile")

    System.out.println(dropWhile(xy, (x: Int) => x < 50))

=======
    System.out.println(drop(5, xy));
>>>>>>> 17b5a25b40c50838a36fd4660eddde5820879527
  }

}